function validateEmail()
{
    let email=document.getElementById("email");
    let comment=document.getElementById("textArea-comment");
    let element=document.getElementById("error");
    let emailRegex=/^([_\-\.0-9a-zA-Z]+)@([_\-\.0-9a-zA-Z]+)\.([a-zA-Z]){2,7}$/;

    if(!emailRegex.test(email.value))
    {
        email.style.borderColor="red";
        element.style="display:block";
        element.textContent="Invalid email adress!";
    }
    else if(comment.value=='')
    {
        email.style.borderColor="black";
        comment.style.borderColor="red";
        element.textContent="";
        element.textContent="Text area must be filled!";
        element.style="display:block";
    }
    else
    {
        element.style="display:none";
        element.textContent="";
        comment.style.borderColor="black";
        email.style.borderColor="black";
    }
}

function checkEmail()
{    
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = function() {
    const myObj = JSON.parse(this.responseText);
    document.getElementById("test").style.color=myObj.color;
    document.getElementById("test").innerHTML = myObj.description;
    document.getElementById("email").style.borderColor=myObj.color;
    }
    let e=document.getElementById("email").value;
    console.log(e);
    xmlhttp.open("GET", "contact-page/checkemail.php?email="+e);
    xmlhttp.send();
}

