<?php
    $email=array("success"=>"", "color"=>"", "description"=>"");
    $pom=$_GET['email'];
    if(filter_var($pom,FILTER_VALIDATE_EMAIL))
    {
        $email['success']="true";
        $email['color']="green";
        $email['description']="Valid email!";
    }
    else
    {
        $email['success']="false";
        $email['color']="red";
        $email['description']="Invalid email!";
    }

    $myobject=json_encode($email);
    echo $myobject;
?>