<?php
    $username=isset($username)?$username:'';
    $email=isset($email)?$email:'';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../style.css">
    

    <title>Login</title>
  </head>
  <body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="../pages/home.php">Home</a>
                </li>
                <li>
                    <a href="">FAQ</a>
                </li>
                <li>
                    <a href="../pages/contact.php">Contact</a>
                </li>
                <li>
                    <a href="../pages/shop.php">Shop</a>
                </li>
                <section id="menu-right">
                <li>        
                    <a href="../pages/login.php"> Login </a>
                </li>
                <li>        
                    <a href="../functionality/controller.php?action=logout"> Logout </a>
                </li>
                <li>        
                    <a href="../pages/orders.php"> Cart </a>
                </li>
                <select name="currency" id="currency">
                    <option value="usd">USD</option>
                    <option value="eur">EUR</option>
                    <option value="rsd">RSD</option>
                </select>
                <select name="language" id="language">
                    <option value="eng">English</option>
                    <option value="srb">Serbian</option>
                    <option value="frn">French</option>
                </select>
            </section>
            </ul>
        </nav>
        <section id="logo">
            <div>
                <h1><img src="../image/geometric.png" class="icons" alt=""> technogy</h1>
            </div>
            <div>
                <p>Send us a message</p>
                <a href="">demo@demo.com</a>
            </div>
            <div>
                <p>Username: <?=$username?></p>
                <p>Email: <?=$email?></p>
            </div>
            <div id="icon-right">
                <img class="icons" src="../image/user.png" alt="" >
                <img class="icons" src="../image/heart.png" alt=""><p> 0</p>
                <img class="icons" src="../image/shop.png" alt=""><p> 0</p>
            </div>
        </section>
        <hr>
        <section id="menu2">
            <section>
                <select name="categories" id="categories">
                    <option selected disabled>BROWSE CATEGORIES</option>
                    <option value="cat1">Category 1</option>
                    <option value="cat2">Category 2</option>
                    <option value="cat3">Category 3</option>
                </select>
                <select name="home" id="home">
                    <option selected disabled>HOME</option>
                    <option value="h1">Opt 1</option>
                    <option value="h2">Oon 2</option>
                    <option value="h3">Oon 3</option>
                    <option value="h4">Oin 4</option>
                </select>
                <select  name="shop" id="shop">
                    <option selected disabled>SHOP</option>
                    <option value="s1">Opt 1</option>
                    <option value="s2">Opt 2</option>
                    <option value="s3">Opt 3</option>
                    <option value="s4">Opt 4</option>
                </select>
                <select name="pages" id="pages">
                    <option selected disabled>PAGES</option>
                    <option value="p1">Opt 1</option>
                    <option value="p2">Opt 2</option>
                    <option value="p3">Opt 3</option>
                    <option value="p4">Opt 4</option>
                </select>
                <a href="">BLOGS</a>
                <a href="contact.html">CONTACTS</a>
            </section>
            <div class="search-container">
                <form>
                  <input type="text" onkeyup="showSearched()" placeholder="Search our catalog" id="search-bar" name="search">
                  <button type="submit"> <img src="../image/search-interface-symbol.png" height="14px" width="14px"> </button>
                </form>
            </div>
        </section>
    </header>
