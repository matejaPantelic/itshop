<?php 
    include_once '../partials/header.php';
?>
<main>
    <div id="contact-container">
        <section id="store-information">
            <h2>STORE INFORMATION</h2>
            <div>
                <img src="../image/location.png" class="store-information-icon">
                <p>Leo Technoloy <br> United States</p>
            </div>
            <hr>
            <div>
                <img src="../image/mail.png" class="store-information-icon">
                <p>Email us: <br> demo@demo.com</p>
            </div>
        </section>
        <section id="main-container">
            <h2>Our location</h2>
            <section id="contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2875.5879320773943!2d20.35232301544488!3d43.885088579113976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475772388c694f27%3A0x4841a3dfdeb39771!2sQuantox%20Technology!5e0!3m2!1ssr!2srs!4v1650057295616!5m2!1ssr!2srs" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </section>
            <section id="contact-form">
                <h2>Drop Us A Line</h2>
                <p>Have a question or comment? Use the form below to send us a message or contact us by mail.</p>
                <p id="error"></p>
                <form>
                    <select name="customer-service" id="">
                        <option value="act" disabled selected>Customer service</option>
                        <option value="opt1">Option 1</option>
                        <option value="opt2">Option 2</option>
                        <option value="opt3">Option 3</option>
                        <option value="opt4">Option 4</option>
                        <option value="opt5">Option 5</option>
                    </select>
                    <input onchange="checkEmail()" id="email" type="text" name="email" placeholder="your@email.com">
                    <p id="test"></p>
                    <br>
                    <div>
                    <input  type="file" name="input-file">
                    </div>
            
                    <textarea name="" id="textArea-comment" cols="30" rows="10" placeholder="How can we help?"></textarea>
                    <input type="button" value="Submit comment"> <!-- onclick="validateEmail()"> -->
                </form>
            </section>
        </section>
    </div>
</main>

<?php 
    include_once '../partials/footer.php';
?>

<script src="validation-email.js"></script>
