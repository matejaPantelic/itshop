<?php 
    include_once '../partials/header.php';
?>    

<main>
    <section id="first">
        <div>
            <div>
            <p>MIDWEEK DEALS</p>
            <h3>SMARTPHONES ACCESSORIES</h3>
            <p>Up to 40% off</p>
            <input type="button" value="Shop now">
            </div>
            <img id="huaweip50" src="../image/huaweip50.jpeg" alt="">
        </div>
        <div>
            <div>
            <p>LIMITED STOCK</p>
            <h3>MACBOOK AIR</h3>
            <p>Starfing from $900.99</p>
            <input type="button" value="Shop now">
            </div>
            <img id="huaweip50" src="../image/macbook.jpeg" class="iphone" alt="">
        </div>
        <div>
            <div>
            <p>DISCOVER</p>
            <h3>BEAT PRO X3</h3>
            <p>$259.99 $900.99</p>
            <input type="button" value="Shop now">
            </div>
            <img id="huaweip50" src="../image/headphone.jpeg" class="iphone" alt="">
        </div>
    </section>
    <section id="second">
        <div>
                <h2>Our featured Offers</h2>
                <p>View All ></p>
        </div>
        <div>
            <div class="card">
                <img src="../image/samsunga51.jpg" alt="" class="card-img">
                <div>
                    <p>TABLETS</p>
                    <h4>Samsung Galaxy M51</h4>
                    <img src="../image/rating.png" alt="" class="rating-img">
                    <p>$23.90 <span>$19.12</span></p>
                </div>
            </div>
            <div class="card">
                <img src="../image/lenovoidea.jpeg" alt="" class="card-img">
                <div>
                    <p>MOBILES</p>
                    <h4>Lenovo IdeaPad S540 13.3"</h4>
                    <img src="../image/rating.png" alt="" class="rating-img">
                    <p>$23.90 <span>$28.72</span></p>
                </div>
            </div>
            <div class="card">
                <img src="../image/tvled.jpeg" alt="" class="card-img">
                <div>
                    <p>AIR CONDITIONERS</p>
                    <h4>TV LED LG Full HD 43 Inch 43LK5000</h4>
                    <img src="../image/rating.png" alt="" class="rating-img">
                    <p>$29.00 <span>$27.55</span></p>
                </div>
            </div>
            <div class="card">
                <img src="../image/hppavilion.png" alt="" class="card-img">
                <div>
                    <p>AIR CONDITIONERS</p>
                    <h4>HP Pavilion X360 Convertibile 14-Dy0064TU</h4>
                    <img src="../image/rating.png" alt="" class="rating-img">
                    <p><span>$29.00</span></p>
                </div>
            </div>
        </div>
    </section>
    <section id="third">
        <div>
            <div>
                <h2>This Week Deals</h2>
                <p>365days : 21hours : 31min : 12sec</p>
            </div>
            <p>View All Deals ></p>
        </div>
        <div>
            <div>
                <img src="../image/samsunga51.jpg" alt="" class="card-img">
                <div>
                    <p>TABLETS</p>
                    <h4>Samsung Galaxy M51</h4>
                    <img src="../image/rating.png" alt="" class="rating-img">
                    <p>$23.90 <span>$19.12</span></p>
                </div>
            </div>
            <div>
                <img src="../image/lenovoidea.jpeg" alt="" class="card-img">
                <div>
                    <p>MOBILES</p>
                    <h4>Lenovo IdeaPad S540 13.3"</h4>
                    <img src="../image/rating.png" alt="" class="rating-img">
                    <p>$23.90 <span>$28.72</span></p>
                </div>
            </div>
        </div>
    </section>
    <section id="sponsors">
        <img src="../image/quantox-logo.png" alt="" class="sponsors-img">
        <img src="../image/quantox-logo.png" alt="" class="sponsors-img">
        <img src="../image/quantox-logo.png" alt="" class="sponsors-img">
        <img src="../image/quantox-logo.png" alt="" class="sponsors-img">
        <img src="../image/quantox-logo.png" alt="" class="sponsors-img">
        <img src="../image/quantox-logo.png" alt="" class="sponsors-img">
    </section>
</main>

    <?php
    include_once '../partials/footer.php';
    ?>